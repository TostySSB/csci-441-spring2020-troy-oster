#ifndef MATRIX_4
#define MATRIX_4

#include <cmath>
#include <iostream>

#define SCALE 1
#define TRANSLATION 2
#define ROTATION 3

#define ROT_X 1
#define ROT_Y 2
#define ROT_Z 3
#define ORTH 1
#define PERS 2
class vec4
{
private:
    float vals[4];

public:
    //Constructor
    vec4()
    {
        vals[0] = vals[1] = vals[2] = vals[3] = 0;
    }
    vec4(float x, float y, float z, float w)
    {
        vals[0] = x;
        vals[1] = y;
        vals[2] = z;
        vals[3] = w;
    }
    //Credit for this operator notation https://www.3dgep.com/understanding-the-view-matrix/

    // I also used this link to understand how a lot of this worked, and it discussed this overloading method, it seemed like the best solution
    const float &operator[](int idx) const
    {
        return vals[idx];
    }
    float &operator[](int idx)
    {
        return vals[idx];
    }
};
class Mat4
{
private:
    vec4 cols[4];

public:
    //Init to identity matrix
    Mat4()
    {
        cols[0] = vec4(1, 0, 0, 0);
        cols[1] = vec4(0, 1, 0, 0);
        cols[2] = vec4(0, 0, 1, 0);
        cols[3] = vec4(0, 0, 0, 1);
    }
    Mat4(int mode)
    {
        //CREDIT: DANA helped me with this

        if (mode == ORTH)
        {
            const float l = -1.0; // left
            const float r = 1.0;  // right
            const float b = -1.0; // bottom
            const float t = 1.0;  // top
            const float n = .1;   // near
            const float f = -100;  // far
            cols[0] = vec4(2 / (r - l), 0, 0, 0);
            cols[1] = vec4(0, 2 / (t - b), 0, 0);
            cols[2] = vec4(0, 0, 2 / (n - f), 0);
            cols[3] = vec4(
                -(r + l) / (r - l),
                -(t + b) / (t - b),
                -(f + n) / (n - f),
                1.0);
        }
        else if (mode == PERS)
        {
            const float l = -1.0; // left
            const float r = 1.0;  // right
            const float b = -1.0; // bottom
            const float t = 1.0;  // top
            const float n = -1.0; // near
            const float f = 1.0;  // far
            cols[0] = vec4(n, 0, 0, 0);
            cols[1] = vec4(0, n, 0, 0);
            cols[2] = vec4(0, 0, f + n, 1);
            cols[3] = vec4(0, 0, -n * f, 0);
        }
        else
        {
            std::cout << "\n\nInvalid mode (" << mode << ")"
                      << "\nPlease enter either PERSPECTIVE or ORTHOGONAL"
                      << std::endl;
            exit(1);
        }
    }
    //Scale or translation
    Mat4(int mode, float x, float y, float z)
    {
        switch (mode)
        {
        case SCALE:
            if (x == 0)
            {
                cols[0] = vec4(1, 0, 0, 0);
                cols[1] = vec4(0, 1, 0, 0);
                cols[2] = vec4(0, 0, 1, 0);
                cols[3] = vec4(0, 0, 0, 1);
            }
            else
            {
                cols[0] = vec4(x / 100, 0, 0, 0);
                cols[1] = vec4(0, y / 100, 0, 0);
                cols[2] = vec4(0, 0, z / 100, 0);
                cols[3] = vec4(0, 0, 0, 1);
            }

            break;
        case TRANSLATION:
            cols[0] = vec4(1, 0, 0, x / 100);
            cols[1] = vec4(0, 1, 0, y / 100);
            cols[2] = vec4(0, 0, 1, z / 100);
            cols[3] = vec4(0, 0, 0, 1);
            break;
        default:
            break;
        }
    }

    //Rotation
    Mat4(float rot_angle, int mode)
    {
        switch (mode)
        {
        case ROT_X:
            cols[0] = vec4(1, 0, 0, 0);
            cols[1] = vec4(0, cos(rot_angle), sin(rot_angle), 0);
            cols[2] = vec4(0, -sin(rot_angle), cos(rot_angle), 0);
            cols[3] = vec4(0, 0, 0, 1);
            break;
        case ROT_Y:
            cols[0] = vec4(cos(rot_angle), 0, -sin(rot_angle), 0);
            cols[1] = vec4(0, 1, 0, 0);
            cols[2] = vec4(sin(rot_angle), 0, cos(rot_angle), 0);
            cols[3] = vec4(0, 0, 0, 1);
            break;
        case ROT_Z:
            cols[0] = vec4(cos(rot_angle), sin(rot_angle), 0, 0);
            cols[1] = vec4(-sin(rot_angle), cos(rot_angle), 0, 0);
            cols[2] = vec4(0, 0, 1, 0);
            cols[3] = vec4(0, 0, 0, 1);
            break;
        default:
            break;
        }
    }

    Mat4(vec4 x, vec4 y, vec4 z, vec4 w)
    {
        cols[0] = x;
        cols[1] = y;
        cols[2] = z;
        cols[3] = w;
    }

    void printMatrix()
    {
        for (int x = 0; x < 4; x++)
        {
            for (int y = 0; y < 4; y++)
            {
                std::cout << cols[y][x] << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }

    const vec4 &operator[](int idx) const
    {
        return cols[idx];
    }
    vec4 &operator[](int idx)
    {
        return cols[idx];
    }
    vec4 operator*(const vec4 &v)
    {
        return vec4(
            cols[0][0] * v[0] + cols[1][0] * v[1] + cols[2][0] * v[2] + cols[3][0] * v[3],
            cols[0][1] * v[0] + cols[1][1] * v[1] + cols[2][1] * v[2] + cols[3][1] * v[3],
            cols[0][2] * v[0] + cols[1][2] * v[1] + cols[2][2] * v[2] + cols[3][2] * v[3],
            cols[0][3] * v[0] + cols[1][3] * v[1] + cols[2][3] * v[2] + cols[3][3] * v[3]);
    }
    // Credit for multiplication strategy: https://www.3dgep.com/understanding-the-view-matrix/
    Mat4 operator*(const Mat4 &m)
    {
        vec4 X(
            cols[0][0] * m[0][0] + cols[1][0] * m[0][1] + cols[2][0] * m[0][2] + cols[3][0] * m[0][3],
            cols[0][1] * m[0][0] + cols[1][1] * m[0][1] + cols[2][1] * m[0][2] + cols[3][1] * m[0][3],
            cols[0][2] * m[0][0] + cols[1][2] * m[0][1] + cols[2][2] * m[0][2] + cols[3][2] * m[0][3],
            cols[0][3] * m[0][0] + cols[1][3] * m[0][1] + cols[2][3] * m[0][2] + cols[3][3] * m[0][3]);
        vec4 Y(
            cols[0][0] * m[1][0] + cols[1][0] * m[1][1] + cols[2][0] * m[1][2] + cols[3][0] * m[1][3],
            cols[0][1] * m[1][0] + cols[1][1] * m[1][1] + cols[2][1] * m[1][2] + cols[3][1] * m[1][3],
            cols[0][2] * m[1][0] + cols[1][2] * m[1][1] + cols[2][2] * m[1][2] + cols[3][2] * m[1][3],
            cols[0][3] * m[1][0] + cols[1][3] * m[1][1] + cols[2][3] * m[1][2] + cols[3][3] * m[1][3]);
        vec4 Z(
            cols[0][0] * m[2][0] + cols[1][0] * m[2][1] + cols[2][0] * m[2][2] + cols[3][0] * m[2][3],
            cols[0][1] * m[2][0] + cols[1][1] * m[2][1] + cols[2][1] * m[2][2] + cols[3][1] * m[2][3],
            cols[0][2] * m[2][0] + cols[1][2] * m[2][1] + cols[2][2] * m[2][2] + cols[3][2] * m[2][3],
            cols[0][3] * m[2][0] + cols[1][3] * m[2][1] + cols[2][3] * m[2][2] + cols[3][3] * m[2][3]);
        vec4 W(
            cols[0][0] * m[3][0] + cols[1][0] * m[3][1] + cols[2][0] * m[3][2] + cols[3][0] * m[3][3],
            cols[0][1] * m[3][0] + cols[1][1] * m[3][1] + cols[2][1] * m[3][2] + cols[3][1] * m[3][3],
            cols[0][2] * m[3][0] + cols[1][2] * m[3][1] + cols[2][2] * m[3][2] + cols[3][2] * m[3][3],
            cols[0][3] * m[3][0] + cols[1][3] * m[3][1] + cols[2][3] * m[3][2] + cols[3][3] * m[3][3]);

        return Mat4(X, Y, Z, W);
    }
};

vec4 normalize(vec4 v)
{
    float magnitude = sqrt(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2));
    // std::cout << "MAGNITUDE "<< magnitude << std::endl;
    vec4 normal_vector;
    if (magnitude != 0)
    {
        normal_vector[0] = v[0] / magnitude;
        normal_vector[1] = v[1] / magnitude;
        normal_vector[2] = v[2] / magnitude;
    }
    // std::cout << "NORMAL VECTOR "<< normal_vector[3] << std::endl;

    return normal_vector;
}

vec4 dot_product(vec4 v1, vec4 v2)
{
    vec4 dot_result;

    dot_result[0] = (v1[1] * v2[2]) - (v1[2] * v2[1]);
    dot_result[1] = (v1[0] * v2[2]) - (v1[2] * v2[0]);
    dot_result[2] = (v1[0] * v2[1]) - (v1[1] * v2[0]);
    // std::cout << "DOT " << dot_result[0] << std::endl;
    return dot_result;
}
#define VEC_X 0
#define VEC_Y 1
#define VEC_Z 2
// CREDIT: https://www.3dgep.com/understanding-the-view-matrix/#The_Camera_Transformation

// Basically helped me do this look at camera, also im not using this right now

Mat4 create_look_at_camera(vec4 eye_position, vec4 target, vec4 up_dir)
{
    // Compute forward vector
    vec4 forward_dir(
        eye_position[0] - target[0],
        eye_position[1] - target[1],
        eye_position[2] - target[2],
        0);
    forward_dir = normalize(forward_dir);
    vec4 zaxis = forward_dir;                           // The "forward" vector.
    vec4 xaxis = normalize(dot_product(up_dir, zaxis)); // The "right" vector.
    vec4 yaxis = dot_product(zaxis, xaxis);             // The "up" vector.

    Mat4 orientation = {
        vec4(xaxis[0], yaxis[0], zaxis[0], 0),
        vec4(xaxis[1], yaxis[1], zaxis[1], 0),
        vec4(xaxis[2], yaxis[2], zaxis[2], 0),
        vec4(0, 0, 0, 1)};

    // Create a 4x4 translation matrix.
    // The eye position is negated which is equivalent
    // to the inverse of the translation matrix.
    // T(v)^-1 == T(-v)
    Mat4 translation = {
        vec4(1, 0, 0, 0),
        vec4(0, 1, 0, 0),
        vec4(0, 0, 1, 0),
        vec4(-eye_position[0], -eye_position[1], -eye_position[2], 1)};

    // Combine the orientation and translation to compute
    // the final view matrix. Note that the order of
    // multiplication is reversed because the matrices
    // are already inverted.
    return (orientation * translation);
}

#endif