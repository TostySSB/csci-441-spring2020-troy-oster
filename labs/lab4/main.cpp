#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix4.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

bool w_is_pressed = false;
bool s_is_pressed = false;
bool bslash_is_pressed = false;

int y_scalar_val = 0;
int trans_x = 0;
int trans_y = 0;
int trans_z = 0;
int scale_val = 100;
int rot_x = 0;
int rot_y = 0;
int rot_z = 0;

int mode = ORTH;

float model_matrix[16] = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1};
Mat4 camera_matrix = Mat4();

void framebufferSizeCallback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

Mat4 test1 = Mat4(
    vec4(5,0,3,1),
    vec4(2,6,8,8),
    vec4(6,2,1,5),
    vec4(1,0,4,6)
);

Mat4 test2 = Mat4(
    vec4(7,1,9,5),
    vec4(5,8,4,3),
    vec4(8,2,3,7),
    vec4(0,6,8,9)
);

Mat4 testmat = test1*test2;


void processInput(GLFWwindow *window, Shader &shader)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
    {
        trans_x--;
        std::cout << "trans x: "<< trans_x << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
    {
        trans_x++;
        std::cout << "trans x: "<< trans_x << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {
        trans_y--;
        std::cout << "trans y: "<< trans_y << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {
        trans_y++;
        std::cout << "trans y: "<< trans_y << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS)
    {
        trans_z--;
        std::cout << "trans z: "<< trans_z << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS)
    {
        trans_z++;
        std::cout << "trans z: "<< trans_z << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_MINUS) == GLFW_PRESS)
    {
        std::cout << "scale: "<< scale_val << std::endl;
        scale_val--;
    }
    if (glfwGetKey(window, GLFW_KEY_EQUAL) == GLFW_PRESS)
    {
        std::cout << "scale: "<< scale_val << std::endl;
        scale_val++;
    }
    if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS)
    {
        rot_x--;
        std::cout << "rot x: "<< rot_x << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
    {
        rot_x++;
        std::cout << "rot x: "<< rot_x << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
    {
        rot_y--;
        std::cout << "rot y: "<< rot_y << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
    {
        rot_y++;
        std::cout << "rot y: "<< rot_y << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_BRACKET) == GLFW_PRESS)
    {
        rot_z--;
        std::cout << "rot z: "<< rot_z << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET) == GLFW_PRESS)
    {
        rot_z++;
        std::cout << "rot z: "<< rot_z << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_PRESS)
    {
        bslash_is_pressed = true;
        std::cout << "BASLASDFA" << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_BACKSLASH) == GLFW_RELEASE && bslash_is_pressed)
    {
        bslash_is_pressed = false;
        if (mode == ORTH){
            mode = PERS;
        }else{
            mode = ORTH;
        }
        std::cout << "MODE BOI: " << mode << std::endl;
    }

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        w_is_pressed = true;
        if (y_scalar_val < 20)
        {
            y_scalar_val++;
        }
    }
    //Dana Parker gave me the idea to include the boolean outside the ifs
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_RELEASE && w_is_pressed)
    {
        w_is_pressed = false;
    }

    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {

        s_is_pressed = true;
        if (y_scalar_val > -20)
        {
            y_scalar_val--;
        }
    }
    //Dana Parker gave me the idea to include the boolean outside the ifs
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_RELEASE && s_is_pressed)
    {
        s_is_pressed = false;
    }
}

void errorCallback(int error, const char *description)
{
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void)
{
    GLFWwindow *window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit())
    {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL())
    {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
        -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
        0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
        -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,

        -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
        -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,

        0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
        0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
        0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f,
        0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,

        -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
        -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f};

    // copy vertex data
    GLuint VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                          (void *)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                          (void *)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        Mat4 rot_x_mat = Mat4(rot_x,ROT_X);
        Mat4 rot_y_mat = Mat4(rot_y, ROT_Y);
        Mat4 rot_z_mat = Mat4(rot_z, ROT_Z);
        Mat4 trans_mat = Mat4(TRANSLATION,trans_x,trans_y,trans_z);
        Mat4 scale_mat = Mat4(SCALE, float(scale_val), float(scale_val), float(scale_val));
        Mat4 proj_mat = Mat4(mode);
        vec4 eye_position = vec4(0, y_scalar_val, -20, 0);
        
        //camera_matrix = create_look_at_camera(eye_position,vec4(0,0,0,1),vec4(0,1,0,0));

        // Genuinely have no idea why this works, I made a camera matrix method following the notes, it did not work in the slightest, then i manually just made this one and it works
        camera_matrix = Mat4(
            vec4(1,0,0,0),
            vec4(0,1,0,y_scalar_val),
            vec4(0,0,1,20),
            vec4(0,0,0,1)
        );
        //camera_matrix.printMatrix();
        Mat4 trans = scale_mat * rot_z_mat * rot_x_mat * rot_y_mat * trans_mat;
        int count = 0;
        float proj_matrix[16];
        float cam_matrix[16];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                model_matrix[count] = trans[j][i];
                proj_matrix[count] = proj_mat[j][i];
                cam_matrix[count] = camera_matrix[j][i];
                count++;
            }
        }
        
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        unsigned int transLoc = glGetUniformLocation(shader.id(), "transmatrix");
        unsigned int projLoc = glGetUniformLocation(shader.id(), "projection");
        unsigned int camLoc = glGetUniformLocation(shader.id(), "camera");
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, proj_matrix);
        glUniformMatrix4fv(transLoc, 1, GL_FALSE, model_matrix);
        glUniformMatrix4fv(camLoc, 1, GL_FALSE, cam_matrix);
        // activate shader
        shader.use();

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
