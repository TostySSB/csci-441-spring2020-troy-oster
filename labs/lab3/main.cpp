/* 
    Collab for ideas: Dana Parker
    Credits: Internet sources are credited and linked in the parts of the code where they are used.
*/
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "matrix_math/matrix.h"
#include "transforms/translation/translation.h"

#define STATIC 0
#define ROTATE_CENTER 1
#define ROTATE_OFF_CENTER 2
#define SCALE 3
#define IMPRESS 4

// float identityMatrix[] = {
//     1, 0, 0,
//     0, 1, 0,
//     0, 0, 1};

// float rotate[] = {
//     1, 0, 0,
//     0, 1, 0,
//     0, 0, 1};
// float scale[] = {
//     1, 0, 0,
//     0, 1, 0,
//     0, 0, 1};
// float translation_matrix[] = {
//     1, 0, 0,
//     0, 1, 0,
//     0, 0, 1};

int mode = 0;

void framebufferSizeCallback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}
bool space_is_pressed = false;
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    {
        
        space_is_pressed = true;
    }
    //Dana Parker gave me the idea to include the boolean outside the ifs
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE && space_is_pressed)
    {
        space_is_pressed = false;
        if (mode < IMPRESS)
        {
            std::cout << "ENTERING MODE: "<< mode + 1 << std::endl;
            mode++;
        }
        else
        {
            std::cout << "ENTERING MODE: 0"<< std::endl;
            mode = STATIC;
        }
    }
}

int main(void)
{
    // Matrix2d l = { {1,2,3,4},
    //              {5,6,7,8},
    //              {9,1,2,3} };
    // Matrix2d n( 3,std::vector<float>(4,0));
    // Matrix2d m( 1,std::vector<float>(2,0));
    // multiply_matrices(n,m);
    // print_matrix2d(l);
    /* Initialize the library */
    GLFWwindow *window;
    if (!glfwInit())
    {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL())
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
        0.5f,
        0.5f,
        1.0,
        0.0,
        0.0,
        0.5f,
        -0.5f,
        1.0,
        1.0,
        0.0,
        -0.5f,
        0.5f,
        0.0,
        1.0,
        0.0,

        0.5f,
        -0.5f,
        1.0,
        1.0,
        0.0,
        -0.5f,
        -0.5f,
        0.0,
        0.0,
        1.0,
        -0.5f,
        0.5f,
        0.0,
        1.0,
        0.0,
    };

    // int arr_size = sizeof(triangle)/sizeof(triangle[0]);
    // Matrix2d tri = convert_to_2d(triangle, arr_size,5);
    // std::cout << "MATRIX:" << std::endl;
    // print_matrix2d(tri);
    // Matrix2d trans = create_translation_matrix(tri, .1, 0);
    // std::cout << "TRANSLATION MATRIX" << std::endl;
    // print_matrix2d(trans);
    // Matrix2d translated_matrix = translate(tri,.1,.2);
    // std::cout << "TRANSLATED MATRIX" << std::endl;
    // print_matrix2d(translated_matrix);
    // std::cout << "ROW VECTOR" << std::endl;
    // Matrix2d row_Matrix = convert_to_row_vectors(translated_matrix);
    // print_matrix2d(row_Matrix);

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(0 * sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5 * sizeof(float), (void *)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        // process input
        processInput(window);
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
        //Using uniform1v because I could not for the life of me understand how uniformMatrix worked
        
        float time = glfwGetTime();

        Matrix2d transform_matrix = create_identity_matrix(3);
        Matrix2d translation_matrix = create_identity_matrix(3);
        Matrix2d rotation_matrix = create_identity_matrix(3);
        Matrix2d scale_matrix = create_identity_matrix(3);
        switch (mode)
        {
        case STATIC:
        {
            break;
        }
        case ROTATE_CENTER:
        {
            /*Rotate around center*/
            rotation_matrix[0][0] = cos(time);
            rotation_matrix[0][1] = -(sin(time));
            rotation_matrix[1][0] = sin(time);
            rotation_matrix[1][1] = cos(time);
            break;
        }
        case ROTATE_OFF_CENTER:
        {

            translation_matrix[0][2] = .8;
            translation_matrix[1][2] = .4;

            rotation_matrix[0][0] = cos(time);
            rotation_matrix[0][1] = -(sin(time));
            rotation_matrix[1][0] = sin(time);
            rotation_matrix[1][1] = cos(time);
            break;
        }
        case SCALE:
        {
            scale_matrix[0][0] = tan(time);
            scale_matrix[1][1] = -tan(time);
            break;
        }
        case IMPRESS:
        {
            scale_matrix[1][1] = sin(time);
            translation_matrix[0][2] = sin(time);
            translation_matrix[1][2] = tan(time);
        }
        default:
            break;
        }
        //This is not using a library, I have basically created my own with overloading in the /matrix_math folder
        transform_matrix = translation_matrix * rotation_matrix;
        transform_matrix = transform_matrix * scale_matrix;
        float arr[9] = {
            transform_matrix[0][0], transform_matrix[0][1], transform_matrix[0][2],
            transform_matrix[1][0], transform_matrix[1][1], transform_matrix[1][2],
            transform_matrix[2][0], transform_matrix[2][1], transform_matrix[2][2]};

        unsigned int transLoc = glGetUniformLocation(shader.id(), "transmatrix");
        glUniformMatrix3fv(transLoc, 1, GL_FALSE, arr);
        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);
        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}
