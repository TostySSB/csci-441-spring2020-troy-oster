#version 330 core
layout(location=0)in vec2 aPos;
layout(location=1)in vec3 aColor;

uniform mat3 transmatrix;

out vec3 myColor;

void main(){
    
    /*
    
    Credit to Apurv Saxena's Blog for this method of matrix multiplication, its a little faster as we do not need to loop through unneccessary matrix items (items not in the top left 4 locations) 
    
    Link: http://apurvsaxena.blogspot.com/2012/09/matrix-multiplication-3x3-and-3x1.html

    */

    float x = (transmatrix[0][0] * aPos.x) + (transmatrix[0][1] * aPos.y) + transmatrix[0][2];
    float y = (transmatrix[1][0] * aPos.x) + (transmatrix[1][1] * aPos.y) + transmatrix[1][2];
     gl_Position = vec4(x,y,0, 1.0);
    myColor=aColor;
}
