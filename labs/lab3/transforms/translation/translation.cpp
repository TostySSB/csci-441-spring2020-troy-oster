#include "translation.h"
#include <algorithm> 
Matrix2d create_translation_matrix(Matrix2d m, float x, float y){
    int const m_rows = m.size(),
              m_cols = m[0].size();
    int row_col_size = std::max(m_rows,m_cols);
    Matrix2d translation_matrix(row_col_size, std::vector<float>(row_col_size, 0));
    
    for (int i = 0; i < row_col_size; i++)
    {
        for (int j = 0; j < row_col_size; j++)
        {
            if(i == j){
                translation_matrix[i][j] = 1;
            }else if (i==0 && j == row_col_size-1)
            {
                translation_matrix[i][j] = x;
            }else if (i==1 && j == row_col_size-1)
            {
                translation_matrix[i][j] = y;
            }
        }
        
    }
    return translation_matrix;
}

Matrix2d translate(Matrix2d m, float x, float y){
    Matrix2d matrix_to_be_translated = convert_to_col_vectors(m);
    Matrix2d translation_matrix =  create_translation_matrix(matrix_to_be_translated,x,y);
    translation_matrix = translation_matrix * matrix_to_be_translated;
    return translation_matrix;
}