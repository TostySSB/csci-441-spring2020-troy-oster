#ifndef TRANSLATION_H

//Credit to Dana Parker for teaching me this file setup
#define TRANSLATION_H

#include "../../matrix_math/matrix.h"
// Credit to user edW on this stack overflow link for this big brain typedef, https://stackoverflow.com/questions/8767166/passing-a-2d-array-to-a-c-function

Matrix2d create_translation_matrix(Matrix2d m, float x, float y);

Matrix2d translate(Matrix2d m, float x, float y);\


#endif