#ifndef MATRIX_H

//Credit to Dana Parker for teaching me this file setup
#define MATRIX_H

#include <vector>

// Credit to user edW on this stack overflow link for this big brain typedef, https://stackoverflow.com/questions/8767166/passing-a-2d-array-to-a-c-function
typedef std::vector< std::vector<float> > Matrix2d;

Matrix2d add_matrices(Matrix2d& m1, Matrix2d& m2);

Matrix2d convert_to_2d(float arr[],int arr_size, int row_len);

Matrix2d convert_to_col_vectors(Matrix2d& m);

Matrix2d convert_to_row_vectors(Matrix2d& m);


float* conv_to_arr(Matrix2d& conv_mat);

Matrix2d create_identity_matrix(int size);

Matrix2d operator*(Matrix2d& m1, Matrix2d& m2);

void print_matrix2d(Matrix2d& m);

#endif