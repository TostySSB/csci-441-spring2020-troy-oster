#include "matrix.h"
#include <iostream>

Matrix2d add_matrices(Matrix2d& m1, Matrix2d& m2)
{
    int const m1_rows = m1.size(),
              m1_cols = m1[0].size(),
              m2_rows = m2.size(),
              m2_cols = m2[0].size();

    Matrix2d add_matrix(m1_rows, std::vector<float>(m2_cols, 0));

    if (m1_rows == m2_rows && m1_cols == m2_cols)
    {
        for (int i = 0; i < m1_rows; i++)
        {
            for (int j = 0; j < m1_cols; j++)
            {
                add_matrix[i][j] += m1[i][j] + m2[i][j];
            }
        }
    }
    else
    {
        std::cout << "Matrices are incompatible: Matrices must have the same dimensions" << std::endl;
        std::cout << "The dimensions of your matrices were:" << std::endl;
        std::cout << "    M1: " << m1_rows << " X " << m1_rows << std::endl;
        std::cout << "    M2: " << m2_rows << " X " << m2_cols << std::endl;
    }
    return add_matrix;
}

Matrix2d convert_to_2d(float arr[], int arr_size, int row_len)
{
    Matrix2d conv_matrix(arr_size / row_len, std::vector<float>(row_len, 0));
    std::cout << "ARR_SIZE" << arr_size << std::endl;
    int count = 0;
    for (int i = 0; i < arr_size / row_len; i++)
    {
        for (int j = 0; j < row_len; j++)
        {
            conv_matrix[i][j] += arr[count];
            count++;
        }
    }
    return conv_matrix;
}

float* conv_to_arr(Matrix2d& conv_matrix){
    int const m_rows = conv_matrix.size(),
              m_cols = conv_matrix[0].size();
    int count = 0;
    float arr[9];
    for (int i = 0; i < m_rows; i++)
    {
        for (int j = 0; j < m_cols; j++)
        {
            arr[count]= conv_matrix[i][j];
            count++;
        }
    }
    std::cout << arr << std::endl;
    return arr;
}

Matrix2d convert_to_col_vectors(Matrix2d& m){
    int const m_rows = m.size(),
              m_cols = m[0].size();
    Matrix2d conv_matrix(m_cols + 1, std::vector<float>(m_rows, 0));
    for (int i = 0; i < m_cols+1; i++)
    {
        for (int j = 0; j < m_rows; j++)
        {
            if (i == m_cols){
                conv_matrix[i][j] = 1;
            }else{
                conv_matrix[i][j] += m[j][i];
            }
        }
    }
    std::cout << "CONV FOR TRANS" << std::endl;
    print_matrix2d(conv_matrix);
    return conv_matrix;
}

Matrix2d convert_to_row_vectors(Matrix2d& m){
    int const m_rows = m.size(),
              m_cols = m[0].size();
    Matrix2d conv_matrix(m_cols, std::vector<float>(m_rows-1, 0));
    for (int i = 0; i < m_cols; i++)
    {
        for (int j = 0; j < m_rows-1; j++)
        {
            conv_matrix[i][j] += m[j][i];
        }
    }
    std::cout << "CONV FOR TRANS" << std::endl;
    print_matrix2d(conv_matrix);
    return conv_matrix;
}

Matrix2d operator* (Matrix2d& m1, Matrix2d& m2)
{

    int const m1_rows = m1.size(),
              m1_cols = m1[0].size(),
              m2_rows = m2.size(),
              m2_cols = m2[0].size();
    Matrix2d mult_matrix(m1_rows, std::vector<float>(m2_cols, 0));
    if (m1_cols != m2_rows)
    {
        std::cout << "Matrices are incompatible:" << std::endl;
        std::cout << "The dimensions of your matrices were:" << std::endl;
        std::cout << "    M1: " << m1_rows << " X " << m1_cols << std::endl;
        std::cout << "    M2: " << m2_rows << " X " << m2_cols << std::endl;
    }
    else
    {
        for (int i = 0; i < m1_rows; i++)
        {
            for (int j = 0; j < m2_cols; j++)
            {
                for (int k = 0; k < m1_cols; k++)
                {
                    mult_matrix[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
    }

    /* 
        Credit: https://www.programiz.com/cpp-programming/examples/matrix-multiplication for matrix multiplication in cpp (in a cleaner solution)
    */

    return mult_matrix;
}
Matrix2d create_identity_matrix(int size){

    Matrix2d ident(size, std::vector<float>(size, 0));
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if(i == j){
                ident[i][j] = 1;
            }
        }
    }
    return ident;
}
void print_matrix2d(Matrix2d &m)
{
    int m_rows = m.size();
    int m_cols = m[0].size();
    std::cout << "Matrix: " << std::endl;
    for (int i = 0; i < m_rows; i++)
    {
        for (int j = 0; j < m_cols; j++)
        {
            std::cout << m[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}
