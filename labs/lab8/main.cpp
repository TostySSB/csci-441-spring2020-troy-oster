#include <iostream>

#include <glm/glm.hpp>

#include <bitmap/bitmap_image.hpp>

#define BACKGROUND_COLOR make_colour(0, 44, 92)

struct Viewport
{
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2 &min, const glm::vec2 &max)
        : min(min), max(max) {}
};

struct Ray
{
    glm::vec3 origin;
    glm::vec3 direction;
    Ray(const glm::vec3 &origin = glm::vec3(0, 0, 0), const glm::vec3 &direction = glm::vec3(0, 0, -1)) : origin(origin), direction(normalize(direction)) {}
};

struct Sphere
{
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3 &center = glm::vec3(0, 0, 0),
           float radius = 0,
           const glm::vec3 &color = glm::vec3(0, 0, 0))
        : center(center), radius(radius), color(color)
    {
        static int id_seed = 0;
        id = ++id_seed;
    }
};
//Credit: David Millman matrix4.h
unsigned int idx(unsigned int r, unsigned int c)
{
    return r + c * 480;
}
//Logic came from user TJank who fixed a bug in the code found here: http://viclw17.github.io/2018/07/16/raytracing-ray-sphere-intersection/, article also provides helpful insight into how ray tracing works
float sphere_collision(const glm::vec3 &center, float radius, const Ray &r)
{
    //printf("DIRECTION: %f,%f,%f   CENTER: %f,%f,%f \n", r.direction.x, r.direction.y, r.direction.z, center.x, center.y, center.z);
    glm::vec3 oc = r.origin - center;
    float a = dot(r.direction, r.direction);
    float b = 2.0 * dot(oc, r.direction);
    float c = dot(oc, oc) - radius * radius;
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0.0)
    {
        return FLT_MAX;
    }
    else
    {
        float numerator = -b - sqrt(discriminant);
        if (numerator > 0.0)
        {
            return numerator / (2.0 * a);
        }

        numerator = -b + sqrt(discriminant);
        if (numerator > 0.0)
        {
            return numerator / (2.0 * a);
        }
        else
        {
            return FLT_MAX;
        }
    }
}
void render(bitmap_image &image, const std::vector<Sphere> &world)
{
    // TODO: implement ray tracer
    float r = 5;
    float l = -5;
    float t = -5;
    float b = 5;
    float nx = 640;
    float ny = 480;
    
    for (int x = 0; x <= image.width(); x++)
    {
        for (int y = 0; y <= image.height(); y++)
        {
            // This boi be makin orthographic rays
            float ui = l + (r - l) * (x + .5) / nx;
            float vj = b + (t - b) * (y + .5) / ny;
            Ray rij(glm::vec3(ui, vj, 0), glm::vec3(0, 0, 1));
            // Initialize the closest collision to MAX, so if its less than it, its closer
            float closest_collision = FLT_MAX;
            glm::vec3 collision_color;
            for (Sphere s : world)
            { //https://www.geeksforgeeks.org/g-fact-40-foreach-in-c-and-java/, I am a javascript kiddo and I like me some for in / for each loops
                float collision_distance = sphere_collision(s.center, s.radius, rij);
                if (collision_distance < closest_collision)
                { // Basically if there is a collision that is closer, change our current color to the color of the sphere the ray collided with
                    closest_collision = collision_distance;
                    collision_color = s.color;
                }
            }
            if (closest_collision != FLT_MAX)
            { // If a collision exists, change the pixel color to the found circle (colliding circle)
                image.set_pixel(x, y, make_colour(collision_color.x * 255, collision_color.y * 255, collision_color.z * 255));
            }else{
                image.set_pixel(x, y, BACKGROUND_COLOR );
            }
        }
    }
    image.save_image("orthographic.bmp");

    for (int x = 0; x <= image.width(); x++)
    {
        for (int y = 0; y <= image.height(); y++)
        {
            // This boi be makin perspective rays
            float ui = l + (r - l) * (x + .5) / nx;
            float vj = b + (t - b) * (y + .5) / ny;
            Ray rij(glm::vec3(0, 0, -5), glm::vec3(ui, vj, 0) - glm::vec3(0, 0, -5));
            // Initialize the closest collision to MAX, so if its less than it, its closer
            float closest_collision = FLT_MAX;
            glm::vec3 collision_color;
            for (Sphere s : world)
            { //https://www.geeksforgeeks.org/g-fact-40-foreach-in-c-and-java/, I am a javascript kiddo and I like me some for in / for each loops
                float d = sphere_collision(s.center, s.radius, rij);
                if (d < closest_collision)
                { // Basically if there is a collision that is closer, change our current color to the color of the sphere the ray collided with
                    closest_collision = d;
                    collision_color = s.color;
                }
            }
            if (closest_collision != FLT_MAX)
            { // If a collision exists, change the pixel color to the found circle (colliding circle)
                image.set_pixel(x, y, make_colour(collision_color.x * 255, collision_color.y * 255, collision_color.z * 255));
            }else{
                image.set_pixel(x, y, BACKGROUND_COLOR );
            }
        }
    }
    image.save_image("perspective.bmp");
}

int main(int argc, char **argv)
{

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1, 1, 0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0, 1, 1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1, 0, 1)),
        Sphere(glm::vec3(-1, 1, 4), 2, glm::vec3(0, 1, 1)),
        Sphere(glm::vec3(-2, 2, 6), 3, glm::vec3(1, 0, 1)),
    };

    // render the world
    render(image, world);

    std::cout << "Success" << std::endl;
}
