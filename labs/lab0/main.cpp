
#include <iostream>
#include <string>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }
    Vector3() : x(0), y(0), z(0) {
    }
    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

Vector3 add(const Vector3& a, const Vector3& b){
    return Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector3 operator+(const Vector3& v, const Vector3& v2){
    return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
}
std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
        // std::cout is a std::ostream object, just like stream
        // so use stream as if it were cout and output the components of
        // the vector
        stream << v.x << "," << v.y << "," << v.z;
        return stream;
}
int main(int argc, char** argv) {
    std::string name;
    //Print all command line arguments
    for (int x = 0; x < argc; x++){
        std::cout << "Command Line Argument " << x << ":" << std::endl;
        std::cout << argv[0] << std::endl;
    }
    std::cout << "What is your name?" << std::endl;
    std::cin >> name;
    std::cout << "hello " << name << std::endl;

    Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    Vector3 c = a + b; // copies 6 floats to the arguments in add (3 per Vector3),
                          // 3 more floats copied into c when it returns
                          // a and b are unchanged
    std::cout << "Vector C: (" << c.x << "," << c.y << "," << c.z <<")" << std::endl;
    
    std::cout << a+b << std::endl;

    std::cout << "Part 7" << std::endl;
    
    Vector3 d(0,0,0);
    Vector3* pointer_to_d = &d;
    pointer_to_d->y = 5;
    std::cout << "Part 7 Vector: " << std::endl;
    std::cout << d << std::endl;
    std::cout << "Part 7 Heap Vector: " << std::endl;

    Vector3* e = new Vector3(0,0,0);
    e->y = 5;
    std::cout << *e << std::endl;

    //Part 8
    Vector3 array[10];
    Vector3* array_heap = new Vector3[10];
    for (int x = 0; x < 10; x++){
        std::cout << "Vector " << x << " in heap array: " << std::endl;
        array_heap[x].y = 5;
        std::cout << array_heap[x] << std::endl;
    }
    delete [] array_heap;
    return 0;
}

