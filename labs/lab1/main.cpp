
#include <iostream>
#include "bitmap_image.hpp"

#define X_VAL 0
#define Y_VAL 1
#define R 2
#define G 3
#define B 4
#define WHITE make_colour(255, 255, 255)

using namespace std;

/* 
    Collab: Dana Parker, showed me how to cin with spaces and also taught me about macros
    Credit: User Logic on StackOverflow and his blog post http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html for info on barycentric coordinates
*/




void getUserInput(vector<float>& p){
    cout << "Enter a point in the format << x y r g b >>" << endl;
    float x,y,r,g,b;
    if (cin >> x >> y >> r >> g >> b) {
    // add x y r g and b to vector
        p.push_back(x);
        p.push_back(y);
        p.push_back(r);
        p.push_back(g);
        p.push_back(b);
    }
}

vector<float> get_bounding_box(vector<float> vArr[]){
    vector<float> tempVect;
    tempVect.push_back(min(vArr[0].at(X_VAL), min(vArr[1].at(X_VAL),vArr[2].at(X_VAL))));
    tempVect.push_back(min(vArr[0].at(Y_VAL), min(vArr[1].at(Y_VAL),vArr[2].at(Y_VAL))));
    tempVect.push_back(max(vArr[0].at(X_VAL), max(vArr[1].at(X_VAL),vArr[2].at(X_VAL))));
    tempVect.push_back(max(vArr[0].at(Y_VAL), max(vArr[1].at(Y_VAL),vArr[2].at(Y_VAL))));
    return tempVect;
}

void draw_bounding_box(vector<float> b_box,bitmap_image& image, vector<float> vArr[]){
    float a,b,c;
    const float x1 = vArr[0].at(X_VAL);
    const float x2 = vArr[1].at(X_VAL);
    const float x3 = vArr[2].at(X_VAL);
    const float y1 = vArr[0].at(Y_VAL);
    const float y2 = vArr[1].at(Y_VAL);
    const float y3 = vArr[2].at(Y_VAL);
    const float r1 = vArr[0].at(R) * 255;
    const float r2 = vArr[1].at(R) * 255;
    const float r3 = vArr[2].at(R) * 255;
    const float g1 = vArr[0].at(G) * 255;
    const float g2 = vArr[1].at(G) * 255;
    const float g3 = vArr[2].at(G) * 255;
    const float b1 = vArr[0].at(B) * 255;
    const float b2 = vArr[1].at(B) * 255;
    const float b3 = vArr[2].at(B) * 255;
    float r,g,b_color;
    for(int x = b_box.at(0); x<b_box.at(2); x++){
        for(int y = b_box.at(1); y<b_box.at(3); y++){
            a = ((y2-y3)*(x-x3)+(x3-x2)*(y-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
            b = ((y3-y1)*(x-x3)+(x1-x3)*(y-y3))/((y2-y3)*(x1-x3)+(x3-x2)*(y1-y3));
            c = 1 - a - b;

            r = a*r1 + b*r2 + c*r3;
            g = a*g1 + b*g2 + c*g3;
            b_color = a*b1 + b*b2 + c*b3;
            rgb_t color = make_colour(r,g,b_color);
            
            if(a>=0 && a<=1){
                if(b>=0 && b<=1){
                    if(c>=0 && c<=1){
                        image.set_pixel(x,y,color);
                    }
                }
            }
        }
    }
}

int main(int argc, char** argv) {
    /*
      Prompt user for 3 points separated by whitespace.

      Part 1:
          You'll need to get the x and y coordinate as floating point values
          from the user for 3 points that make up a triangle.

      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);
    vector<float> vArr[3];
    for (int x=0; x<3 ; x++){
        getUserInput(vArr[x]);
    }
    vector<float> bounding_box = get_bounding_box(vArr);
    draw_bounding_box(bounding_box, image, vArr);

    /*
      Part 1:
          Calculate the bounding box of the 3 provided points and loop
          over each pixel in that box and set it to white using:

          rgb_t color = make_color(255, 255, 255);
          image.set_pixel(x,y,color);

      Part 2:
          Modify your loop from part 1. Using barycentric coordinates,
          determine if the pixel lies within the triangle defined by
          the 3 provided points. If it is color it white, otherwise
          move on to the next pixel.

      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("triangle.bmp");
    cout << "Success" << endl;
}
